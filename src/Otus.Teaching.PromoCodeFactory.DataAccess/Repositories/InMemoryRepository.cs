﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Infrastructure;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult((IEnumerable<T>)Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<Guid> AddAsync(T newItem)
        {
            return Task.Run(() =>
            {
                newItem.Id = Guid.NewGuid();
                Data.Add(newItem);
                return newItem.Id;
            });
        }

        public Task<ErrorEnum> ChangeAsync(T newItem)
        {
            return Task.Run(() =>
            {
                var item = Data.FirstOrDefault(x => x.Id == newItem.Id);
                if (item == null)
                    return ErrorEnum.NotFound;
                item.Update(newItem);
                return ErrorEnum.None;
            });
        }

        public Task<ErrorEnum> RemoveByIdAsync(Guid id)
        {
            return Task.Run(() =>
            {
                var item = Data.FirstOrDefault(x => x.Id == id);
                if (item == null)
                    return ErrorEnum.NotFound;
                if (Data.Remove(item))
                    return ErrorEnum.None;
                return ErrorEnum.NotFound;
            });
        }
    }
}