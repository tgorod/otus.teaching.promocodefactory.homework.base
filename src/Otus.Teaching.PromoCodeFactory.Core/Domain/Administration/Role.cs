﻿using System;
using System.Data;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role
        : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public override void Update(BaseEntity entity)
        {
            if (entity as Role == null)
                return;
            var role = entity as Role;
            // заменяем только те значения, которые пришли на вход
            if (role.Name != null)
                Name = role.Name;
            if (role.Description != null)
                Description = role.Description;
        }
    }
}