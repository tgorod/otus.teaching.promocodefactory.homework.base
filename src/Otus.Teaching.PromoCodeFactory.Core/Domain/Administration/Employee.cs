﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee
        : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public List<Role> Roles { get; set; }

        public int AppliedPromocodesCount { get; set; }

        public override void Update(BaseEntity entity)
        {
            if (entity as Employee == null)
                return;
            var employee = entity as Employee;
            AppliedPromocodesCount = employee.AppliedPromocodesCount;
            // заменяем только те значения, которые пришли на вход
            if (employee.FirstName != null)
                FirstName = employee.FirstName;
            if (employee.LastName != null)
                LastName = employee.LastName;
            if (employee.Email != null)
                Email = employee.Email;
            if (employee.Roles != null)
                Roles = employee.Roles;
        }
    }
}